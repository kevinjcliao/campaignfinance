package example

import org.scalatest._

class BudgieSpec extends FlatSpec with Matchers {
  "readState" should "convert appropriately." in {
    Budgie.readState("ACT") shouldEqual ACT
  }
}
