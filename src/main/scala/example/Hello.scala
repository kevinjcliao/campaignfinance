package example

import scala.io.Source

sealed trait AusState
case object ACT extends AusState
case object NSW extends AusState
case object NT extends AusState
case object QLD extends AusState
case object SA extends AusState
case object TAS extends AusState
case object VIC extends AusState
case object WA extends AusState
case object FED extends AusState
case object MalformedState extends AusState

// Glossary:
// Coalition: Liberals, Nationals.
// ALP: Labor
// PHON: One Nation
// CDP: Christian Democratic Party (Fred Nile Group)
// AusDems: Australian Democrats
// CECOA: Citizens' Electoral Council of Australia
// NXT: Nick Xenophon Team
// EL: Emily's List
// FEF: Free Enterprise Foundation
// PBA: Progressive Business Association
// FHC: 500 Hundred Club
// FFP: Family First Party
// PBI: Progressive Business Incorporated
sealed trait Party
case object Coalition extends Party
case object Greens extends Party
case object ALP extends Party
case object PHON extends Party
case object CDP extends Party
case object AusDems extends Party
case object CECOA extends Party
case object NXT extends Party
case object EL extends Party
case object FEF extends Party
case object PBA extends Party
case object FHC extends Party
case object FFP extends Party
case object PBI extends Party
case object Unsorted extends Party

case class Donor(
  name: String,
  address: Option[String],
  suburb: Option[String],
  state: Option[AusState],
  postCode: Option[String]
)

case class Donation (
  donor: String,
  recipient: String,
  date: String,
  amount: Double
)

case class DonationRow(
  donor: Donor,
  donation: Donation,
  recipient: Party
)

object Budgie extends App {
  type Recipient = String
  type DonationsByDonor = Map[Donor, List[Donation]]
  type DonationsByParty = Map[Recipient, List[Donation]]

  val filenames = List(
    "data/1998-1999-AnalysisDonor.csv",
    "data/1999-2000-AnalysisDonor.csv",
    "data/2000-2001-AnalysisDonor.csv",
    "data/2001-2002-AnalysisDonor.csv",
    "data/2002-2003-AnalysisDonor.csv",
    "data/2003-2004-AnalysisDonor.csv",
    "data/2004-2005-AnalysisDonor.csv",
    "data/2005-2006-AnalysisDonor.csv",
    "data/2006-2007-AnalysisDonor.csv",
    "data/2007-2008-AnalysisDonor.csv",
    "data/2008-2009-AnalysisDonor.csv",
    "data/2009-2010-AnalysisDonor.csv",
    "data/2010-2011-AnalysisDonor.csv",
    "data/2011-2012-AnalysisDonor.csv",
    "data/2012-2013-AnalysisDonor.csv",
    "data/2013-2014-AnalysisDonor.csv",
    "data/2014-2015-AnalysisDonor.csv",
    "data/2015-2016-AnalysisDonor.csv",
    "data/2016-2017-AnalysisDonor.csv"
  )

  def readState(line: String): AusState = line match {
    case "ACT" => ACT
    case "NSW" => NSW
    case "NT"  => NT
    case "QLD" => QLD
    case "SA"  => SA
    case "TAS" => TAS
    case "VIC" => VIC
    case "WA"  => WA
    case _     => MalformedState
  }

  def toNoneAndCorrect[a](line: String, corrector: String => a): Option[a] = line match {
    case "" => None
    case a  => Some(corrector(a))
  }

  // There are sometimes typos in the suburb names. Declare corrections here.
  def readSuburb(line: String): String = line match {
    case "Macquaire Park" => "Macquarie Park"
    case a                => a
  }

  def readParty(line: String): Party = line match {
    case lnp if line contains "LIBERAL" => Coalition
    case alp if line contains "ALP"     => ALP
    case grn if line contains "GREEN"   => Greens
    case alp if line contains "LABOR PARTY"   => ALP
    case nat if line contains "NATIONAL PARTY OF AUSTRALIA" => Coalition
    case phon if line contains "ONE NATION" => PHON
    case cdp if line contains "CHRISTIAN DEMOCRATIC PARTY" => CDP
    case adp if line contains "AUSTRALIAN DEMOCRATS" => AusDems
    case cec if line contains "CITIZENS ELECTORAL COUNCIL" => CECOA
    case nxt if line contains "XENEPHON" => NXT
    case nxt if line contains "NXT" => NXT
    case emi if (line contains "EMILY") && (line contains "LIST") => EL
    case fef if line contains "FREE ENTERPRISE FOUNDATION" => FEF
    case pba if line contains "PROGRESSIVE BUSINESS ASSOCIATION" => PBA
    case fhc if line contains "500 CLUB" => FHC
    case ffp if line contains "FAMILY FIRST PARTY" => FFP
    case alp if line contains "LABOUR PARTY" => ALP
    case lnp if line matches("^.*LIB\\s*-\\s*...?.*$") => Coalition
    case lnp if line matches("^.*NATIONAL PARTY OF...?.*$") => Coalition
    case lnp if line matches("^.*NAT\\s*-\\s*...?.*$") => Coalition
    case lnp if line contains "PROGRESSIVE BUSINESS INCORPORATED" => PBI
    case _ => Unsorted
  }

  def splitString (input: String): Array[String] = input.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")

  def removeFirstAndLastQuotes(col: String): String = col.replaceAll("^\"|\"$", "")

  def readDonation(line: String): DonationRow = {
    val cols = splitString(line).map(_.trim).map(removeFirstAndLastQuotes)
    val donorName = cols(0).toUpperCase
    val donorAddress = toNoneAndCorrect[String](cols(1), s => s.toUpperCase)
    val donorSuburb = toNoneAndCorrect[String](cols(2), s => readSuburb(s).toUpperCase)
    val donorState = toNoneAndCorrect[AusState](cols(3), readState)
    val donorPostCode = toNoneAndCorrect(cols(4), identity)
    val recipient = cols(5).toUpperCase
    val donationDate = cols(6)
    val amount = cols(7).toDouble

    val donor = new Donor(donorName, donorAddress, donorSuburb, donorState, donorPostCode)
    val donation = new Donation(donorName, recipient, donationDate, amount)

    new DonationRow(donor, donation, readParty(recipient))
  }

  def combineToMap[t]
    (accessor: (DonationRow => t))
    (a: Map[t, List[Donation]],
    b: DonationRow): Map[t, List[Donation]] =
    a get accessor(b) match {
        case Some(donations) => a + (accessor(b) -> (b.donation :: donations))
        case None => a + (accessor(b) -> List(b.donation))
    }

  val donationsByYear = filenames.map {
    filename => {
      val bufferedSource = io.Source.fromFile(filename)
      bufferedSource
        .getLines
        .drop(3)
        .map(readDonation)
        .toList
    }
  }

  val donations = donationsByYear.flatten
  val donationsByDonor =
    donations.foldLeft(Map[Donor, List[Donation]]())(combineToMap[Donor](a => a.donor))

  val donationsByParty =
    donations.foldLeft(Map[Party, List[Donation]]())(combineToMap[Party](a => a.recipient))

  val donationsLength = donations.length
  val numDonors = donationsByDonor.size

  // for ((donor, donations) <- donationsByDonor) {
  //   val donorName = donor.name
  //   val donationsSize = donations.size
  //   println(s"$donorName with $donationsSize donations.")
  // }


  donationsByParty get Unsorted match {
    case Some(donations) => donations.foreach(donation => println(s"Unsorted Donation: $donation"))
    case None => println("Does not contain unsorted!")
  }


  def getAverageDonation(ls: List[Donation]): Double =
    ls.foldLeft(0.0)(_ + _.amount) / ls.length

  def getTotalDonation(ls: List[Donation]): Double =
    ls.foldLeft(0.0)(_ + _.amount)

  for ((party, donations) <- donationsByParty) {
    val partyName = party
    val donationsSize = donations.size
    val averageDonation = getAverageDonation(donations)
    val totalDonations = getTotalDonation(donations)
    println(s"$partyName with $donationsSize donations with average donation size: $averageDonation and total donations: $totalDonations")
  }


  println(s"There are $donationsLength donations.")
  println(s"There are $numDonors donors.")
}
